<?php

namespace App\Http\Controllers;

use App\Models\Bitcoin;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BitcoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url='https://bitpay.com/api/rates';
        $json=json_decode(file_get_contents($url));
        $btc=0;

        foreach( $json as $obj ){
            if( $obj->code=='USD' ) $btc = $obj->rate;
        }

        $date = Carbon::now();
        $date = $date->format('Y-m-d');

        $bitcoin = new Bitcoin;

        $bitcoin->valor = $btc;
        $bitcoin->fecha = $date;
        $bitcoin->save();

        return response()->json(['status' => 200, 'data' => $btc]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        if(!$request->filled('fechaini') || !$request->filled('fechafin')){
            return response()->json(['status' => 400, 'message' => 'Ingrese fecha de inicio y fecha de termino']);
        }

        $fechaini = $request['fechaini'];
        $fechafin = $request['fechafin'];

        $data = DB::table('bitcoin')->select('valor', 'created_at as fecha')->whereBetween('fecha',[$fechaini,$fechafin])->get();

        return response()->json(['status' => 200, 'data' => $data]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
