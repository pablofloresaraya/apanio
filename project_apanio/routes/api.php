<?php

use App\Http\Controllers\BitcoinController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/index",[BitcoinController::class,'index']);
Route::post("/report",[BitcoinController::class,'store']);

/*Route::get('/users', function (Request $request) {
    $users = User::paginate(10);
    return $users;
});*/