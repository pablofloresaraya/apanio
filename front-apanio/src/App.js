import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle'; //tabs
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import Home from './_components/home'; 

function App() {
  return (
    <div className="container">
      <Home />
      <ToastContainer />
    </div>
  );
}

export default App;
