import React, { useState, useEffect } from 'react';
import { config } from '../_constants';
import DataTable from './table/DataTable';

const Home = () => {

    const [initTime, setInitTime] = useState(0);
    const [bitcoin, setBitcoin] = useState('');

    const _handleOnGetValue = async () => {

        try {
            const requestOptions = {
                method: 'GET'
            };
            const url = `${config.backendUrl}/index`;
            const response = await window.fetch(url, requestOptions);
            const resp = await response.json();                      
            if(resp.status===200){
                setBitcoin(resp.data);           
            }else{                                          
                console.log('error');                   
            }      
        }
        catch(error){
            console.error('Error:', error);
        };

    }

    useEffect(() => {        
        const interval = setInterval(() => {
            setInitTime(initTime+1);    
        }, 1000);

        return () => {
            clearInterval(interval);
        };    
    },[initTime]);

    useEffect(() => {
        if(initTime===10){
            _handleOnGetValue();
            setInitTime(0); 
        }
    },[initTime]);

    useEffect(() => {        
        _handleOnGetValue();
    },[]);
    
    return(<>
            <div className="card">
                <div className="card-header card-header-primary text-white">
                    <strong>Bitcoin USD</strong>
                </div>
                <div className="card-body">
                    <div className="m-1">
                        <ul  className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link active" href="#bitcoin" data-bs-toggle="tab">Bitcoin</a>
                            </li>
                            <li>
                                <a className="nav-link" href="#historial" data-bs-toggle="tab">Historial</a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <div className="tab-pane fade show active mt-1" id="bitcoin">
                                <div className="row mt-3 justify-content-center">
                                    <div className="col-auto">
                                        <h2>Valor Bitcoin USD:</h2>
                                    </div>
                                    <div className="col-auto">
                                        <h2>{bitcoin}</h2>
                                    </div>
                                </div>    
                            </div>
                            <div className="tab-pane fade" id="historial">
                                <DataTable />                  
                            </div>
                        </div>                        
                    </div>
                </div>  
            </div>
    </>)
}

export default Home;