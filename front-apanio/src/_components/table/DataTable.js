import React, { useState, useEffect } from 'react';
import { config } from '../../_constants';
import { Form } from 'react-bootstrap';
import Pagination from './Pagination';
import SessionSelect from './SessionSelect';
import { toast } from 'react-toastify';

const DataTable = () => {

    const dayjs = require('dayjs')
    const [state, setState] = useState({fechaini:'', fechafin:''});
    const [data, setData] = useState([]);
    const [dataFilter, setDataFilter] = useState([]);
    const [sessionsPerPage, setSessionsPerPage] = useState(5);
    const [currentPage, setCurrentPage] = useState(1);
    //const [limitedSessions, setLimitedSessions] = useState([]);

    const _handleOnChangeText = e => {

        const { name, value } = e.target;

        setState((prevState) => ({
            ...prevState,
            [name]: value
        }));

    }

    const showToastMessageError = msg => {
        toast.error(msg, {
            position: toast.POSITION.TOP_RIGHT,
            className: 'toast-error-container toast-error-container-after'
        });
    };

    const handleGetData = async () => {

        try {
            const requestOptions = {
                method: 'POST'
                ,headers: {"Content-Type" : "application/json", "accept" : "application/json"}
                ,body: JSON.stringify(state)                    
            };

            const url = `${config.backendUrl}/report`;
            const response = await window.fetch(url, requestOptions);
            const resp = await response.json();           
            if(resp.status===200){ 
                (resp.data.length>0) ? setData(resp.data) : showToastMessageError("No existen registros!!");                            
            }else{
                if(resp.status===400){
                    showToastMessageError(resp.message); 
                }
            }      
        }
        catch(error){
            console.error('Error:', error);
        };
    }

    const lastSessionNumber = ((parseInt(sessionsPerPage) * parseInt(currentPage)));
    const firstSessionIndex = (parseInt(lastSessionNumber) - parseInt(sessionsPerPage));

    useEffect(() => {
        if(data.length){  
            setDataFilter(data);
        }  
    },[data]);

    return(
        <>
            <div className='row mt-3 mb-2'>
                <label htmlFor="fechaini" className="col-auto text-start">Fecha inicio</label>
                <div className="col-sm-1 col-md-1 col-lg-2">
                    <Form.Control name="fechaini" type="date" max={state.fechafin} className="form-control form-control-sm text-center" value={state.fechaini} onChange={_handleOnChangeText} /> 
                </div>
                <label htmlFor="fechafin" className="col-auto text-start">Fecha fin</label>
                <div className="col-sm-1 col-md-1 col-lg-2">
                    <Form.Control name="fechafin" type="date" min={state.fechaini} className="form-control form-control-sm text-center" value={state.fechafin} onChange={_handleOnChangeText} />
                </div>
                <div className="col-auto">
                    <button type="button" className="btn btn-primary btn-sm" onClick={handleGetData} disabled={(!state.fechaini || !state.fechafin)} >                        
                        <i className="fas fa-search" />&nbsp;Buscar
                    </button>
                </div>
            </div>
            <div className='table-responsive'>                        
                <table className='table table-bordered table-striped' style={{'width':'100%'}}>
                    <thead>
                        <tr>
                            <th>Valor USD</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        { dataFilter.slice(firstSessionIndex, lastSessionNumber).map((item, index) => {                                    
                            return( 
                                <tr key={(index+1)} >                                    
                                    <td className='text-center'>{item.valor}</td>
                                    <td className='text-center'>{dayjs(item.fecha).format('DD-MM-YYYY HH:mm:ss')}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div className='row'>                            
                    <div className='col-sm-11 col-md-11 col-lg-11'>
                        <Pagination
                            itemsCount={dataFilter.length}
                            itemsPerPage={sessionsPerPage}
                            currentPage={currentPage}
                            setCurrentPage={setCurrentPage}
                            alwaysShown={false}
                        />
                    </div>
                    <div className='col-sm-1 col-md-1 col-lg-1'>
                        {(dataFilter.length>5) && <SessionSelect 
                                                        itemsPerPage={sessionsPerPage}
                                                        setItemsPerPage={setSessionsPerPage}
                                                    />
                        }
                    </div>
            </div>
        </>
    )

}

export default DataTable;