import React from "react";
import PropTypes from "prop-types";

const options = [
  { value: 3, label: "3" },
  { value: 5, label: "5" },
  { value: 10, label: "10" },
  { value: 20, label: "20" },
  { value: 50, label: "50" }
];

const SessionSelect = ({ itemsPerPage, setItemsPerPage }) => {
  
  const onItemsPerPageChange = event => {
    const { value } = event.target;
    setItemsPerPage(Number(value));
  };

  return (
    
        <select className="form-select form-select-sm text-center" aria-label=".form-select-sm" value={itemsPerPage} onChange={onItemsPerPageChange} >
            {options.map(({ value, label }) => (
                <option key={value} value={value}>
                {label}
                </option>
            ))}
        </select>
      
  );
};

SessionSelect.propTypes = {
    itemsPerPage: PropTypes.number.isRequired,
    setItemsPerPage: PropTypes.func.isRequired
  };
  
  export default SessionSelect;